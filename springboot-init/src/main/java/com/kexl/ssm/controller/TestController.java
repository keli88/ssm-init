package com.kexl.ssm.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @classname: TestController
 * @description:
 * @author: kexl
 * @create: 2021-03-16 13:36
 * @version: 1.0
 **/
@RestController
public class TestController {


    @RequestMapping("test")
    public String test(){
        return "ok";
    }
}

